FROM registry.gitlab.com/jcaromiq/corona-bot:dependencies as builder
WORKDIR /usr/src/myapp

COPY src src
RUN cargo build --release

FROM rust:slim
COPY --from=builder /usr/src/myapp/target/release/corona_bot /usr/local/bin/corona_bot
CMD ["corona_bot"]
