.PHONY: build-docker

build-docker:
	rm -rf target
	docker build -t corona-bot .

clippyfy:
	cargo clippy

format:
	cargo fmt

release:
	cargo build --release
