# COVID-19 Telegram bot

## What is this?
COVID-19 bot is a telegram bot to know the current status of the pandemic for a given country.
It shows you information of 'Total cases', 'Total deaths', 'Today cases', 'Today deaths' and 'Total recovered'.

## How to use
Search @coronaVid_bot in telegram or go to [t.me/coronaVid_bot](t.me/coronaVid_bot).

Write the name of the country in english for country data or write total to see the global data. 
  
## Build your self

Can compile and run this bot by cargo:

TELEGRAM_BOT_TOKEN=XXXXX cargo run release
