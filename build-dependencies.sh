#!/bin/sh

set -eux

IMAGE=registry.gitlab.com/jcaromiq/corona-bot
VERSION=dependencies
docker build -t "${IMAGE}":"${VERSION}" --file dependencies.Dockerfile .
docker push "${IMAGE}":"${VERSION}"
