#!/bin/sh

set -eux

IMAGE=${CI_REGISTRY_IMAGE:-localhost:5000/corona-bot}
VERSION=${CI_COMMIT_SHORT_SHA:-latest}
docker build -t "${IMAGE}":"${VERSION}" .
docker push "${IMAGE}":"${VERSION}"
