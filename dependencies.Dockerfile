FROM rustlang/rust:nightly
WORKDIR /usr/src/myapp
ADD Cargo.toml Cargo.lock /usr/src/myapp/
RUN mkdir -p /usr/src/myapp/src && echo "fn main() {}" > /usr/src/myapp/src/main.rs && \
    cargo build --release
