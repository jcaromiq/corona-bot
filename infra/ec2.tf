resource "aws_instance" "covidbot" {
  ami                  = var.ami
  instance_type        = "t2.micro"
  key_name             = var.key_name
  iam_instance_profile = "ecsInstanceRole"

  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id]

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file(var.pem_file)
    host        = self.public_dns
  }

  provisioner "remote-exec" {
    inline = [
      "curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash",
      "sudo yum update -y && sudo yum install docker -y && sudo yum install gitlab-runner -y",
      "sudo usermod -aG docker $USER",
      "sudo usermod -aG docker gitlab-runner",
      "sudo service docker start",
      "sudo yum install gitlab-runner -y",
      "sudo gitlab-runner register --non-interactive --url \"https://gitlab.com/\" --registration-token \"${var.gitlab_token}\" --executor \"shell\" "
    ]
  }

  tags = {
    Name = "Covidbot"
  }
}


resource "aws_iam_policy" "policy" {
  name = "ECS-CloudWatchLogs"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams"
            ],
            "Resource": [
                "arn:aws:logs:*:*:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach" {
  role       = "ecsInstanceRole"
  policy_arn = aws_iam_policy.policy.arn
}
