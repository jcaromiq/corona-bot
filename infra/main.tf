provider "aws" {
  region = "eu-west-1"
}

output "ecs_public_dns" {
  value = aws_instance.covidbot.public_dns
}
