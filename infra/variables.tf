variable "gitlab_token" {
  description = "gitlab ci token"
}

variable "pem_file" {
  description = "full path to pem file"
}

variable "ami" {
  default     = "ami-0bb3fad3c0286ebd5"
  description = "ec2 ami"
}

variable "key_name" {
  default     = "covid-bot"
  description = "key name"
}

variable "default_vpc" {
  default     = "vpc-28d22a51"
  description = "default region vpc"
}
