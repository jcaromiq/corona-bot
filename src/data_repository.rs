use chrono::{TimeZone, Utc};
use reqwest::{Error, StatusCode};
use serde_derive::Deserialize;

pub async fn get_data_for_country(country: &str) -> Result<Data, ApiError> {
    let url = format!("{}{}", "https://corona.lmao.ninja/v2/countries/", country);

    let resp = reqwest::Client::new().get(&url).send().await?;
    match resp.status() {
        StatusCode::OK => {
            let data = to_data(resp.json().await?);

            if data.is_error() {
                Err(ApiError::new(format!(
                    "Received response status: {:?} calling: {}",
                    StatusCode::NOT_FOUND, url
                )))
            } else {
                Ok(data)
            }
        }
        status_code => Err(ApiError::new(format!(
            "Received response status: {:?} calling: {}",
            status_code, url
        ))),
    }
}

pub async fn get_total_data() -> Result<Data, ApiError> {
    let url = String::from("https://corona.lmao.ninja/v2/all");

    let resp = reqwest::Client::new().get(&url).send().await?;
    match resp.status() {
        StatusCode::OK => Ok(to_data(resp.json().await?)),
        status_code => Err(ApiError::new(format!(
            "Received response status: {:?} calling: {}",
            status_code, url
        ))),
    }
}

#[derive(Debug, Clone)]
pub struct ApiError {
    pub error: String,
}

impl ApiError {
    fn new(error: String) -> Self {
        ApiError { error }
    }
}

impl std::convert::From<reqwest::Error> for ApiError {
    fn from(convert_error: Error) -> Self {
        ApiError {
            error: (format!("Error converting data {:?}", convert_error)),
        }
    }
}

fn to_data(json: serde_json::Value) -> Data {
    let updated_at = if let Some(time) = json["updated"].as_i64() {
        Utc.timestamp_millis(time).to_rfc2822()
    } else {
        String::from("")
    };
    Data {
        cases: json["cases"].as_i64().unwrap_or(0),
        tests: json["tests"].as_i64().unwrap_or(0),
        today_cases: json["todayCases"].as_i64().unwrap_or(0),
        deaths: json["deaths"].as_i64().unwrap_or(0),
        today_deaths: json["todayDeaths"].as_i64().unwrap_or(0),
        recovered: json["recovered"].as_i64().unwrap_or(0),
        active: json["active"].as_i64().unwrap_or(0),
        critical: json["critical"].as_i64().unwrap_or(0),
        updated: updated_at,
        message: String::from(json["message"].as_str().unwrap_or("")),
    }
}

#[derive(Debug, Deserialize)]
pub struct Data {
    pub cases: i64,
    pub tests: i64,
    #[serde(rename = "todayCases")]
    pub today_cases: i64,
    pub deaths: i64,
    #[serde(rename = "todayDeaths")]
    pub today_deaths: i64,
    pub recovered: i64,
    pub active: i64,
    pub critical: i64,
    message: String,
    pub updated: String,
}

impl Data {
    pub fn is_error(&self) -> bool {
        !self.message.is_empty()
    }
}
