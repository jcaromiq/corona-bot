use log4rs::{
    append::console::ConsoleAppender,
    config::{Appender, Root},
};
use log4rs::append::file::FileAppender;
use log4rs::encode::pattern::PatternEncoder;
use log::LevelFilter;
use log::{error, info};
use serde::Serialize;
use std::env;

pub fn init() {

    let env_file_path = env::var("LOG_FILE");

    let pattern = "{{\"level\":\"{l}\",\"time\":\"{d(%Y-%m-%d %H:%M:%S %Z)(utc)}\",\"detail\":{m}}}\n";

    let stdout: ConsoleAppender = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(pattern)))
        .build();

    match env_file_path {
        Ok(file_path) => {
            let logfile = FileAppender::builder()
                .encoder(Box::new(PatternEncoder::new(pattern)))
                .build(format!("{}/corona_bot.log",file_path))
                .unwrap();

            let log_config = log4rs::config::Config::builder()
                .appender(Appender::builder().build("stdout", Box::new(stdout)))
                .appender(Appender::builder().build("logFile", Box::new(logfile)))
                .build(
                    Root::builder()
                        .appender("stdout")
                        .appender("logFile")
                        .build(LevelFilter::Info))
                .unwrap();
            log4rs::init_config(log_config).unwrap();
        },
        Err(_) => {
            let log_config = log4rs::config::Config::builder()
                .appender(Appender::builder().build("stdout", Box::new(stdout)))
                .build(
                    Root::builder()
                        .appender("stdout")
                        .build(LevelFilter::Info))
                .unwrap();
            log4rs::init_config(log_config).unwrap();
        },
    }
}

pub fn error(message: LogFromMessage) {
    let m = serde_json::to_string(&message).expect("Error at serialize");
    error!("{}", m)
}

pub fn info_from(message: LogFromMessage) {
    let m = serde_json::to_string(&message).expect("Error at serialize");
    info!("{}", m)
}

pub fn info(message: LogMessage) {
    let m = serde_json::to_string(&message).expect("Error at serialize");
    info!("{}", m);
}


#[derive(Serialize)]
pub struct LogFromMessage {
    pub from: String,
    pub message: String,
}

#[derive(Serialize)]
pub struct LogMessage {
    pub message: String,
}
