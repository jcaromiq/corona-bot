extern crate corona_bot;


mod data_repository;
mod logger;
mod telegram_client;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    logger::init();
    logger::info(logger::LogMessage { message: "Covid bot started!".to_string() });
    telegram_client::init().await?;
    Ok(())
}
