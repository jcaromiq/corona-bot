use std::env;

use futures::StreamExt;
use telegram_bot::{Api, CanReplySendMessage, Error, Message, MessageKind, ParseMode, UpdateKind};
use thousands::Separable;

use lazy_static::lazy_static;

use crate::data_repository::Data;
use crate::logger;
use crate::logger::LogFromMessage;

use super::data_repository;

lazy_static! {
    static ref TELEGRAM: Api = {
        let token = env::var("TELEGRAM_BOT_TOKEN").expect("TELEGRAM_BOT_TOKEN not set");
        Api::new(token)
    };
}

pub async fn init() -> Result<(), Error> {
    let mut stream = TELEGRAM.stream();
    while let Some(update) = stream.next().await {
        if let UpdateKind::Message(message) = update?.kind {
            if let MessageKind::Text { data: ref message_data, .. } = message.kind {
                logger::info_from(LogFromMessage {
                    message: message_data.to_string(),
                    from: message.from.first_name.to_string(),
                });
                execute_command(&message, message_data).await
            }
        }
    }
    Ok(())
}

async fn execute_command(message: &Message, message_data: &str) {
    match message_data.to_lowercase().as_str() {
        "/start" | "/help" | "help" | "?" => send_info(&message).await,
        "total" | "world" | "global" | "worldwide" => total_info(&message).await,
        _ => country_info(&message, message_data).await,
    }
}

async fn total_info(message: &Message) {
    match data_repository::get_total_data().await {
        Ok(response) => send_totals(response, &message).await,
        Err(e) => {
            logger::error(LogFromMessage {
                from: message.from.first_name.to_string(),
                message: e.error,
            });
            send_error(&message).await;
        }
    }
}

async fn country_info(message: &Message, message_data: &str) {
    match data_repository::get_data_for_country(message_data).await {
        Ok(response) => send(response, message_data, &message).await,
        Err(e) => {
            logger::error(LogFromMessage {
                from: message.from.first_name.to_string(),
                message: e.error,
            });
            send_error(&message).await;
        }
    }
}

async fn send_info(message: &Message) {
    let help_message =
        "For get the status for a given country write the name in english of country, ej: Spain\n\
    For get the global status write worldwide, world, total or global";
    if TELEGRAM.send(message.text_reply(help_message)).await
        .is_err() {
        logger::error(LogFromMessage {
            from: "Error sending help message".to_string(),
            message: format!("{:?}", message.from),
        });
    }
}

async fn send_error(message: &Message) {
    if TELEGRAM
        .send(message.text_reply("Country not found or doesn't have any cases"))
        .await
        .is_err()
    {
        logger::error(LogFromMessage {
            from: "Error sending help message".to_string(),
            message: format!("{:?}", message.from),
        });
    }
}

async fn send(data: Data, country: &str, message: &Message) {
    let mut response = message.text_reply(format_by_country_message(country, data));
    if TELEGRAM
        .send(response.parse_mode(ParseMode::Html))
        .await
        .is_err()
    {
        logger::error(LogFromMessage {
            from: "Error sending country data".to_string(),
            message: format!("{:?}", message.from),
        });
    }
}

async fn send_totals(data: Data, message: &Message) {
    let mut response = message.text_reply(format_total_message(data));
    if TELEGRAM
        .send(response.parse_mode(ParseMode::Html))
        .await
        .is_err()
    {
        logger::error(LogFromMessage {
            from: "Error sending totals data".to_string(),
            message: format!("{:?}", message.from),
        });
    }
}

fn format_total_message(data: Data) -> String {
    format!(
        "Updated at {}:\n\
                        Total cases: {}\n\
                        Total deaths: {}\n\
                        Active: {}\n\
                        Critical: {}\n\
                        Tests: {}\n\
                        Total recovered: {}\n",
        data.updated,
        format_number(data.cases),
        format_number(data.deaths),
        format_number(data.active),
        format_number(data.critical),
        format_number(data.tests),
        format_number(data.recovered),
    )
}

fn format_by_country_message(country: &str, data: Data) -> String {
    format!(
        "<b>{}</b> updated at {}:\n\
                        Total cases: {}\n\
                        Total deaths: {}\n\
                        Active: {}\n\
                        Critical: {}\n\
                        Tests: {}\n\
                        Today cases: {}\n\
                        Today deaths: {}\n\
                        Total recovered: {}\n",
        country.to_uppercase(),
        data.updated,
        format_number(data.cases),
        format_number(data.deaths),
        format_number(data.active),
        format_number(data.critical),
        format_number(data.tests),
        format_number(data.today_cases),
        format_number(data.today_deaths),
        format_number(data.recovered),
    )
}

fn format_number(number: i64) -> String {
    format!("{}", number).separate_with_dots()
}
