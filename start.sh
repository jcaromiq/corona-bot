#!/bin/sh

set -eu

IMAGE=${IMAGE:-localhost:5000/corona-bot}
VERSION=${VERSION:-latest}
ENV=${ENV:-dev}
LOG_FILE=${LOG_FILE:-/var/logs/corona}
docker rm --force corona_bot_"${ENV}" || true
docker run -d \
  --log-driver=awslogs \
  --log-opt awslogs-region=eu-west-1 \
  --log-opt awslogs-group=coronaBot"${ENV}" \
  --log-opt awslogs-create-group=true \
  -e TELEGRAM_BOT_TOKEN="${BOT_TOKEN}" \
  -e LOG_FILE="${LOG_FILE}" \
  -v /home/ec2-user/logs/"${ENV}"/:"${LOG_FILE}" \
  --name corona_bot_"${ENV}" \
  --restart always \
  "${IMAGE}":"${VERSION}"
