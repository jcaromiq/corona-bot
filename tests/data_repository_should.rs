extern crate corona_bot;

use pretty_assertions::assert_eq;

use corona_bot::data_repository::get_data_for_country;

#[tokio::test]
async fn get_data_given_a_valid_country() {
    let result = get_data_for_country("spain").await;

    assert_eq!(result.is_ok(), true);
}

#[tokio::test]
async fn get_error_given_a_non_valid_country() {
    let result = get_data_for_country("mordor").await;
    assert_eq!(result.is_err(), true);
}

#[tokio::test]
async fn get_error_given_a_invalid_url() {
    let result = get_data_for_country("/data/more/mordor").await;
    assert_eq!(result.is_err(), true);
}
